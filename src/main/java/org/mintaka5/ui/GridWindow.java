package org.mintaka5.ui;

import javax.swing.*;
import java.awt.*;

public class GridWindow extends JFrame {
    private final int[][] theGrid;

    private int[] dimensions = {10, 10};

    public GridWindow(int[][] randGrid, int[] dims) {
        super("grids");

        theGrid = randGrid;

        dimensions  = dims;

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultLookAndFeelDecorated(true);
        setLocationByPlatform(true);
        setLayout(new BorderLayout());
        add(new IslandsGridPanel(), BorderLayout.CENTER);
        setSize(1024, 640);

        setVisible(true);
    }

    private class IslandsGridPanel extends JPanel {
        public IslandsGridPanel() {

        }

        public void paint(Graphics g) {

            g.setColor(Color.BLUE);

            for(int i=0; i<theGrid.length; i++) {
                for(int j=0; j<theGrid[i].length; j++) {
                    if(theGrid[i][j] == 1) g.setColor(Color.GREEN);
                    else g.setColor(Color.BLUE);

                    int x = i*dimensions[0];
                    int y = j*dimensions[1];

                    g.fillRect(x, y, dimensions[0], dimensions[1]);
                }
            }
        }
    }
}
