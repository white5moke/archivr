package org.mintaka5.ui;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.mintaka5.crypto.PGPTool;
import org.mintaka5.util.Utilities;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.Security;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class PGPWindow extends JFrame {
    public final static String PUBLIC_KEY_PREFIX = "public-";

    public final static String SECRET_KEY_PREFIX = "sercret-";

    private JButton genBtn;

    private JPasswordField passwdTxt;

    private JTextField identTxt;

    private Path keysPath;

    private JList<String> chainList;

    private String[] filenames;

    private JLabel userIdentLbl;
    private PGPPublicKey currentEncryptionKey;
    private JPasswordField passwdPrompt;
    private PGPPrivateKey currentDecryptionKey;
    private JDialog passwdFrame;
    private JLabel keyCreatedLbl;

    public PGPWindow(Path p) throws IOException, PGPException {
        super("pgp thing");

        // setup window
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setLayout(new BorderLayout());
        setSize(640, 480);
        // setSize(960, 720);
        // setSize(1024, 768);

        // set up instance
        keysPath = p;

        // operations
        // set up directory for storage
        if(!Files.exists(keysPath)) {
            Files.createDirectories(keysPath);
        }

        // add ui stuff below this line
        // key generation panel
        buildkeyGenPanel();
        // key details panel
        buildKeyInfoPanel();
        // keychain list panel
        buildKeychainPanel();

        setVisible(true);
    }

    private void buildKeyInfoPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        panel.setLayout(gbl);

        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(new JLabel("identifier"));

        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        userIdentLbl = new JLabel();
        panel.add(userIdentLbl, gbc);

        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.weightx = 0;
        panel.add(new JLabel("created"));

        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.weightx = 1;
        keyCreatedLbl = new JLabel();
        panel.add(keyCreatedLbl, gbc);

        add(panel, BorderLayout.SOUTH);
    }

    private void buildKeychainPanel() throws IOException {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        chainList = new JList<String>();
        JScrollPane chainPane = new JScrollPane(chainList);
        chainList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        chainList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) {
                    String hashId = chainList.getSelectedValue().replaceAll(" @ .*", "");

                    showPasswordPrompt(hashId);
                }
            }
        });
        panel.add(chainPane, BorderLayout.CENTER);
        updateChainList();

        add(panel, BorderLayout.EAST);
    }

    private void showPasswordPrompt(String hashId) {
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        JPanel passwdPanel = new JPanel();
        passwdPanel.setLayout(gbl);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        passwdPrompt = new JPasswordField();
        passwdPrompt.setColumns(25);
        passwdPanel.add(passwdPrompt, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0;
        JButton submitBtn = new JButton("submit");
        passwdPanel.add(submitBtn, gbc);

        passwdFrame = new JDialog(PGPWindow.this, "enter password!", true);

        submitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                char[] passC = passwdPrompt.getPassword();

                if(passC.length > 0) {
                    try {
                        Path pubP = Files.list(keysPath).filter((f) -> f.getFileName().toString().contains(hashId) && f.getFileName().toString().contains(PUBLIC_KEY_PREFIX)).findFirst().get();
                        PGPPublicKeyRing pubRing = PGPTool.importPublicKeyring(pubP.toFile());
                        currentEncryptionKey = PGPTool.getEncryptionKey(pubRing);

                        Path secP = Files.list(keysPath).filter((f) -> f.getFileName().toString().contains(hashId) && f.getFileName().toString().contains(SECRET_KEY_PREFIX)).findFirst().get();
                        PGPSecretKeyRing secRing = PGPTool.importSecretKeyring(secP.toFile());
                        currentDecryptionKey = PGPTool.getDecryptionKey(secRing, currentEncryptionKey.getKeyID(), String.copyValueOf(passC));

                        userIdentLbl.setText(pubRing.getPublicKey().getUserIDs().next());
                        Instant keyInstant = currentEncryptionKey.getCreationTime().toInstant();
                        String keyCreated = DateTimeFormatter.ofPattern("YYYY-mm-dd HH:MM:ss").withZone(ZoneId.systemDefault()).format(keyInstant);
                        keyCreatedLbl.setText(keyCreated);

                        passwdFrame.setVisible(false);
                    } catch (IOException ex) {
                        throw new RuntimeException("unable to find files containing specified hash identifier.".concat(ex.getMessage()));
                    } catch (PGPException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            }
        });

        passwdFrame.setLocationByPlatform(true);
        passwdFrame.getContentPane().add(passwdPanel);
        passwdFrame.pack();
        passwdFrame.setVisible(true);
    }

    private void updateChainList() throws IOException {
        chainList.removeAll();

        filenames = Files.list(keysPath).map(
                (f) -> f.getFileName().toString().replace(".asc", "")
                        .replace(PUBLIC_KEY_PREFIX, "")
                        .replace(SECRET_KEY_PREFIX, "")
                        .concat(" @ ")
                        .concat(
                                DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")
                                        .withZone(ZoneId.systemDefault())
                                        .format(Instant.ofEpochMilli(f.toFile().lastModified())))
        ).distinct().toArray(String[]::new);
        chainList.setListData(filenames);
    }

    private void buildkeyGenPanel() throws PGPException {
        JPanel keyGenPanel = new JPanel();
        GridBagLayout keyGenLayout = new GridBagLayout();
        GridBagConstraints keyGenGBC = new GridBagConstraints();
        keyGenPanel.setLayout(keyGenLayout);

        keyGenGBC.insets = new Insets(5, 5, 5, 5);
        keyGenGBC.fill = GridBagConstraints.BOTH;
        keyGenGBC.gridx = 0;
        keyGenGBC.gridy = 0;
        keyGenPanel.add(new JLabel("identifier"), keyGenGBC);

        keyGenGBC.weightx = 1;
        keyGenGBC.gridx = 1;
        keyGenGBC.gridy = 0;
        identTxt = new JTextField();
        identTxt.setText("chris.is.rad@pm.me");
        keyGenPanel.add(identTxt, keyGenGBC);

        keyGenGBC.weightx = 0;
        keyGenGBC.gridx = 2;
        keyGenGBC.gridy = 0;
        keyGenPanel.add(new JLabel("password"), keyGenGBC);

        keyGenGBC.weightx = 1;
        keyGenGBC.gridx = 3;
        keyGenGBC.gridy = 0;
        passwdTxt = new JPasswordField();
        passwdTxt.setText("ehc121212");
        keyGenPanel.add(passwdTxt, keyGenGBC);

        keyGenGBC.weightx = 0;
        keyGenGBC.gridx = 4;
        keyGenGBC.gridy = 0;
        genBtn = new JButton("generate");
        genBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // disable the panel
                toggleKeyGenPanel(false);

                String idS = identTxt.getText().strip();

                char[] passC = passwdTxt.getPassword();
                String passS = Stream.of(passC).map(String::valueOf).reduce("", String::concat);

                if(!idS.isEmpty() || passC.length > 0) {
                    // go fo launch!
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                // store keys in respective files
                                exportAllKeys(idS, passS);
                                // update the keychain list
                                updateChainList();

                                // reactivate panel
                                toggleKeyGenPanel(true);
                            } catch (IOException | PGPException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    });
                } else {
                    JOptionPane.showMessageDialog(new JFrame(), "please provide both identifier and password!", "key generation error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        keyGenPanel.add(genBtn, keyGenGBC);

        add(keyGenPanel, BorderLayout.NORTH);
    }

    private void toggleKeyGenPanel(boolean b) {
        identTxt.setEnabled(b);
        passwdTxt.setEnabled(b);
        genBtn.setEnabled(b);
    }

    private void exportAllKeys(String id, String pass) throws IOException, PGPException {
        PGPKeyRingGenerator keyRing = PGPTool.generateKeyRing(id, pass);
        Path[] keyFiles = createKeyFilePath(id);
        PGPTool.exportPublicKey(keyRing, keyFiles[0].toFile(), true);
        PGPTool.exportSecretKey(keyRing, keyFiles[1].toFile(), true);
    }

    private Path[] createKeyFilePath(String id) {
        String timeS = Instant.now().toString();
        String ip = Utilities.getWideIp();
        String temp = ip.concat(";").concat(id).concat(";").concat(timeS);

        String hash = Utilities.crc32(temp);

        Path pubP = Path.of(keysPath.toString(), PGPWindow.PUBLIC_KEY_PREFIX.concat(hash).concat(".asc"));
        Path secP = Path.of(keysPath.toString(), PGPWindow.SECRET_KEY_PREFIX.concat(hash).concat(".asc"));

        return new Path[] {pubP, secP};
    }

    public static void main(String[] args) throws IOException, UnsupportedLookAndFeelException, ClassNotFoundException, InstantiationException, IllegalAccessException, PGPException {
        Security.addProvider(new BouncyCastleProvider());
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        new PGPWindow(Path.of("C:", "Users", "chris", "value-is-soul", "pgpkeys"));
    }
}
