package org.mintaka5;

import org.mintaka5.ui.GridWindow;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[][] randGrid = randomGrid(25, 25);
        printGrid(randGrid);
        GridWindow gWin = new GridWindow(randGrid, new int[]{10, 10});
        // new TestingGraphics();
    }

    private static void printGrid(int[][] randGrid) {
        for(int[] row : randGrid) {
            for(int cell : row) {
                System.out.print(cell);
            }

            System.out.println();
        }
    }

    private static int[][] randomGrid(int rowSize, int colSize) {
        int[] vals = {0, 1};
        int[][] arr = new int[rowSize][colSize];

        for(int i=0; i<rowSize; i++) {
            for(int j=0; j<colSize; j++) {
                int r = new Random().nextInt(vals.length);

                arr[i][j] = vals[r];
            }
        }

        return arr;
    }
}