import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class TestingGraphics extends JFrame {
    public TestingGraphics() {
        setSize(1000, 1000);
        setPreferredSize(new Dimension(1000, 1000));
        setTitle("drawing things...");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(new TestingGraphicsPanel(), BorderLayout.CENTER);
        setVisible(true);
    }

    public class TestingGraphicsPanel extends JPanel {
        public TestingGraphicsPanel() {
            setLayout(new BorderLayout());
            setPreferredSize(new Dimension(1000, 1000));
            add(new DrawStuff(), BorderLayout.CENTER);
            revalidate();
            repaint();
            setVisible(true);
        }

        private class DrawStuff extends JComponent {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                Graphics2D g2d = (Graphics2D) g;

                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                Shape rootRect = new Rectangle2D.Float(100, 100, 1000, 500);

                g2d.setColor(Color.BLUE);
                g2d.draw(rootRect);
            }
        }
    }
}
