import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProgressDemo {
    public static void main(String[] main) {
        JFrame frame = new JFrame("invoke demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationByPlatform(true);
        frame.setLayout(new FlowLayout());
        frame.setSize(300, 180);

        JLabel label = new JLabel("download progress goes here!", JLabel.CENTER);
        Thread pretender = new Thread(new ProgressPretender(label));

        JButton simpleButton = new JButton("start");
        simpleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                simpleButton.setEnabled(false);
                pretender.start();
            }
        });

        JLabel checkLabel = new JLabel("can you still type?");
        JTextField checkField = new JTextField(10);

        frame.add(label);
        frame.add(simpleButton);
        frame.add(checkLabel);
        frame.add(checkField);

        frame.setVisible(true);
    }
}

class ProgressPretender implements Runnable {
    JLabel label;
    int progress;

    public ProgressPretender(JLabel lbl) {
        label = lbl;
        progress = 0;
    }

    @Override
    public void run() {
        while(progress <= 100) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    label.setText(progress + "%");
                }
            });

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("someone interrupted us. skipping download");
                break;
            }

            progress++;
        }
    }
}
