import org.mintaka5.net.Client;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class NetTest extends JFrame {
    public NetTest() throws IOException {
        super("NetTest");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setLayout(new BorderLayout());
        setSize(640, 480);

        Client clientPanel = new Client("127.0.0.1", 9000);
        add(clientPanel, BorderLayout.CENTER);

        setVisible(true);
    }
    public static void main(String[] args) throws IOException {
        new NetTest();
    }
}
