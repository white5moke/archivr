import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Hashtable;

public class TestServer {
    private ServerSocket serverSocket;

    public static void main(String[] args) throws IOException {
        int port = 8001;
        new TheServer(port);
    }
}

class TheServer {
    private ServerSocket serverSocket;

    // a mapping from sockets to DataOutputStreams. this will
    // help us avoid having to create a DataOutputStream each time
    // we want to write to astream
    private Hashtable outputStreams = new Hashtable();

    public TheServer(int port) throws IOException {
        listen(port);
    }

    private void listen(int port) throws IOException {
        // create the server's socket
        serverSocket = new ServerSocket(port);
        // tell the world we are ready
        System.out.println("listening on " + serverSocket);

        // keep accepting connections forever
        while(true) {
            // grab next incoming connection
            Socket s = serverSocket.accept();

            // tell the world we've got it
            System.out.println("connection from " + s);

            // create a DataOutputStream for writing data to the
            // other side
            DataOutputStream dOut = new DataOutputStream(s.getOutputStream());

            // save this stream for later use
            outputStreams.put(s, dOut);

            // create a new thread for this connection, and then forget it
            new ServerThread(this, s);
        }
    }

    public Enumeration getOutputStreams() {
        return outputStreams.elements();
    }

    public void sendToAll(String msg) throws IOException {
        // we synchronize this because another thread might be
        // calling removeConnection(), and this would screw things
        // up as we walk through the list
        synchronized (outputStreams) {
            for(Enumeration e = getOutputStreams(); e.hasMoreElements();) {
                // get the output stream
                DataOutputStream dOut = (DataOutputStream) e.nextElement();
                // send the message
                dOut.writeUTF(msg);
            }
        }
    }

    public void removeConnection(Socket s) throws IOException {
        // synchronize so that we don't mess up sendToAll() as
        // it walks the list of output streams
        synchronized (outputStreams) {
            System.out.println("removing connection to " + s);

            // remove from list
            outputStreams.remove(s);

            // make sure it's closed too
            s.close();
        }
    }
}

class ServerThread extends Thread {
    private TheServer theServer;

    // the socket connected to the client
    private Socket socket;

    public ServerThread(TheServer server, Socket s) {
        theServer = server;
        socket = s;

        start();
    }

    public void run() {
        // create a DataInputStream for communicaton; the client
        // is using a DataOutputStream to write to the server
        try {
            DataInputStream dIn = new DataInputStream(socket.getInputStream());

            // forever...
            while(true) {
                // read the next message
                String msg = dIn.readUTF();

                // tell the world
                System.out.println("sending " + msg);

                // send message to all clients
                theServer.sendToAll(msg);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                theServer.removeConnection(socket);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
