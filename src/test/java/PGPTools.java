import org.bouncycastle.bcpg.*;
import org.bouncycastle.bcpg.sig.Features;
import org.bouncycastle.bcpg.sig.KeyFlags;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.bc.BcPGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.PBESecretKeyEncryptor;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyEncryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPDigestCalculatorProvider;
import org.bouncycastle.openpgp.operator.bc.BcPGPKeyPair;
import org.bouncycastle.openpgp.operator.jcajce.*;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Iterator;

public class PGPTools {
    public static String decrypt(byte[] data, PGPPrivateKey privKey) throws IOException, PGPException {
        String content = null;

        InputStream in = PGPUtil.getDecoderStream(new ByteArrayInputStream(data));
        JcaPGPObjectFactory pgpF = new JcaPGPObjectFactory(in);
        PGPEncryptedDataList enc;

        Object o = pgpF.nextObject();
        if(o instanceof PGPEncryptedDataList) {
            enc = (PGPEncryptedDataList) o;
        } else {
            enc = (PGPEncryptedDataList) pgpF.nextObject();
        }

        Iterator<PGPEncryptedData> it = enc.getEncryptedDataObjects();
        PGPPublicKeyEncryptedData pbe = null;

        while(it.hasNext()) {
            pbe = (PGPPublicKeyEncryptedData) it.next();
        }

        InputStream clear = pbe.getDataStream(new JcePublicKeyDataDecryptorFactoryBuilder().setProvider("BC").build(privKey));
        JcaPGPObjectFactory plainF = new JcaPGPObjectFactory(clear);
        Object message = plainF.nextObject();

        if(message instanceof PGPCompressedData) {
            PGPCompressedData cData = (PGPCompressedData) message;
            JcaPGPObjectFactory pgpFact = new JcaPGPObjectFactory(cData.getDataStream());
            message = pgpFact.nextObject();
        }

        if(message instanceof PGPLiteralData) {
            PGPLiteralData ld = (PGPLiteralData) message;
            content = new String(ld.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
        } else {
            throw new PGPException("message is not a simple encrypted file - type unknown!");
        }

        if(pbe.isIntegrityProtected()) {
            if(!pbe.verify()) {
                System.err.println("message failed integrity check");
            } else {
                System.out.println("message passed integrity check");
            }
        }

        return content;
    }

    public static PGPPrivateKey getDecryptionKey(PGPSecretKeyRing secretRing, long pubKeyId, String passwd) throws PGPException {
        Iterator<PGPSecretKey> secretKeys = secretRing.getSecretKeys();

        while(secretKeys.hasNext()) {
            PGPSecretKey sk = secretKeys.next();

            if(sk.getKeyID() == pubKeyId) {
                return sk.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder().setProvider("BC").build(passwd.toCharArray()));
            }
        }

        throw new IllegalArgumentException("no corresponding private key found for supplied ID");
    }

    public static PGPPublicKey getEncryptionKey(PGPPublicKeyRing pubRing) {
        Iterator<PGPPublicKey> pubKeys = pubRing.getPublicKeys();
        while(pubKeys.hasNext()) {
            PGPPublicKey k = pubKeys.next();

            if(k.isEncryptionKey()) {
                return k;
            }
        }

        throw new IllegalArgumentException("no encryption key found in public key ring.");
    }

    public static PGPKeyRingGenerator generateKeyRingGenerator(String id, String passwd) throws PGPException {
        return generateKeyRingGenerator(id, passwd, 0xc0);
    }

    public static PGPKeyRingGenerator generateKeyRingGenerator(String id, String passwd, int s2kCount) throws PGPException {
        RSAKeyPairGenerator kpg = new RSAKeyPairGenerator();
        kpg.init(new RSAKeyGenerationParameters(
                BigInteger.valueOf(0x10001),
                new SecureRandom(),
                4096,
                16
        ));

        // create the master signing key
        PGPKeyPair rsaSign = new BcPGPKeyPair(PGPPublicKey.RSA_SIGN, kpg.generateKeyPair(), new Date());
        // create an encryption subkey
        PGPKeyPair rsaEnc = new BcPGPKeyPair(PGPPublicKey.RSA_ENCRYPT, kpg.generateKeyPair(), new Date());

        // add self-signature on the ID
        PGPSignatureSubpacketGenerator signingHash = new PGPSignatureSubpacketGenerator();
        // add signed metadata to the signature
        // 1) declare its purpose
        signingHash.setKeyFlags(false, KeyFlags.SIGN_DATA | KeyFlags.CERTIFY_OTHER);

        // 2) set preferences for secondary crypto algorithms to use when sending messages
        // to this key.
        signingHash.setPreferredSymmetricAlgorithms(false, new int[] {
                SymmetricKeyAlgorithmTags.AES_256,
                SymmetricKeyAlgorithmTags.AES_192,
                SymmetricKeyAlgorithmTags.AES_128
        });
        signingHash.setPreferredHashAlgorithms(false, new int[] {
                HashAlgorithmTags.SHA256,
                HashAlgorithmTags.SHA1,
                HashAlgorithmTags.SHA384,
                HashAlgorithmTags.SHA512,
                HashAlgorithmTags.SHA224
        });

        // 3) request senders add additional checksums to the message
        // (useful when verifying unsigned messages)
        signingHash.setFeature(false, Features.FEATURE_MODIFICATION_DETECTION);

        // create a signature on the encryption subkey
        PGPSignatureSubpacketGenerator encHash = new PGPSignatureSubpacketGenerator();
        // addthe metadata to declare its purpose
        encHash.setKeyFlags(false, KeyFlags.ENCRYPT_COMMS | KeyFlags.ENCRYPT_STORAGE);

        // objects used to encrypt the secret key
        PGPDigestCalculator sha1Calc = new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA1);
        PGPDigestCalculator sha256Calc = new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA256);

        // protect it!
        PBESecretKeyEncryptor pske = (new BcPBESecretKeyEncryptorBuilder(
                PGPEncryptedData.AES_256,
                sha256Calc, s2kCount
        )).build(passwd.toCharArray());

        // finally, create the keyring itself
        PGPKeyRingGenerator keyRingGen = new PGPKeyRingGenerator(
                PGPSignature.POSITIVE_CERTIFICATION,
                rsaSign,
                id,
                sha1Calc,
                signingHash.generate(),
                null,
                new BcPGPContentSignerBuilder(rsaSign.getPublicKey().getAlgorithm(), HashAlgorithmTags.SHA1),
                pske
        );
        keyRingGen.addSubKey(rsaEnc, encHash.generate(), null);

        return keyRingGen;
    }

    public static void exportSecretKey(PGPKeyRingGenerator gen, File outFile, boolean armored) throws IOException {
        PGPSecretKeyRing secretRing = gen.generateSecretKeyRing();

        if(armored) {
            ArmoredOutputStream aos = new ArmoredOutputStream(new BufferedOutputStream(new FileOutputStream(outFile)));
            secretRing.encode(aos);
            aos.close();
        } else {
            FileOutputStream fos = new FileOutputStream(outFile);
            secretRing.encode(fos);
            fos.close();
        }
    }

    public static void exportPublicKey(PGPKeyRingGenerator gen, File outFile, boolean armored) throws IOException {
        PGPPublicKeyRing pubRing = gen.generatePublicKeyRing();

        if(armored) {
            ArmoredOutputStream aos = new ArmoredOutputStream(new BufferedOutputStream(new FileOutputStream(outFile)));
            pubRing.encode(aos);
            aos.close();
        } else {
            FileOutputStream fos = new FileOutputStream(outFile);
            pubRing.encode(fos);
            fos.close();
        }
    }

    public static PGPPublicKeyRing importPublicKeys(File inFile) throws IOException {
        byte[] inBytes = Files.readAllBytes(inFile.toPath());
        ArmoredInputStream ais = new ArmoredInputStream(new ByteArrayInputStream(inBytes));

        return new PGPPublicKeyRing(ais, new JcaKeyFingerprintCalculator());
    }

    public static PGPSecretKeyRing importSecretKeys(File inFile, String passwd) throws IOException, PGPException {
        byte[] inBytes = Files.readAllBytes(inFile.toPath());
        ArmoredInputStream ais = new ArmoredInputStream(new ByteArrayInputStream(inBytes));

        BcPGPSecretKeyRingCollection keyrings = new BcPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(ais));
        Iterator<PGPSecretKeyRing> secretRings = keyrings.getKeyRings();

        return (PGPSecretKeyRing) secretRings.next();
    }

    public static byte[] encrypt(PGPPublicKey key, byte[] data) throws PGPException, IOException {
        ByteArrayOutputStream encOut = new ByteArrayOutputStream();
        byte[] compressedData = compress(data);

        PGPEncryptedDataGenerator encGen = new PGPEncryptedDataGenerator(
                new JcePGPDataEncryptorBuilder(PGPEncryptedData.CAST5)
                        .setWithIntegrityPacket(true)
                        .setSecureRandom(new SecureRandom())
                        .setProvider("BC")
        );
        encGen.addMethod(new JcePublicKeyKeyEncryptionMethodGenerator(key).setProvider("BC"));

        ArmoredOutputStream aOut = new ArmoredOutputStream(encOut);
        OutputStream cOut = encGen.open(aOut, compressedData.length);
        cOut.write(compressedData);
        cOut.close();
        aOut.close();

        return encOut.toByteArray();
    }

    private static byte[] compress(byte[] data) throws IOException {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(CompressionAlgorithmTags.ZIP);
        OutputStream cos = comData.open(bOut);

        PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
        OutputStream pOut = lData.open(cos, PGPLiteralData.BINARY, PGPLiteralData.CONSOLE, data.length, new Date());
        pOut.write(data);
        pOut.close();

        comData.close();

        return bOut.toByteArray();
    }
}
