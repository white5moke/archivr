package org.mintaka5.net;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class ServerThread extends Thread {
    private Server server;

    private Socket socket;

    public ServerThread(Server server, Socket s) {
        this.server = server;
        socket = s;

        start();
    }

    public void run() {
        try {
            DataInputStream dIn = new DataInputStream(socket.getInputStream());

            while(true) {
                String msg = dIn.readUTF();
                System.out.println("sending `" + msg + "`");
                server.sendToAll(msg);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                server.removeConnection(socket);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
