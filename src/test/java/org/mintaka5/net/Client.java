package org.mintaka5.net;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client extends JPanel implements Runnable {
    private JTextField tf = new JTextField();
    private JTextArea ta = new JTextArea();

    private Socket socket;

    private DataOutputStream dOut;
    private DataInputStream dIn;

    public Client(String host, int port) throws IOException {
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gbl);

        gbc.ipadx = 5;
        gbc.ipady = 5;

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        add(new JLabel("message"), gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(tf, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(ta, gbc);

        tf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    processMessage(e.getActionCommand());
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        // connect to the server
        socket = new Socket(host, port);
        System.out.println("connected to " + socket);

        // grab all the streams and create input/output streams from them
        dIn = new DataInputStream(socket.getInputStream());
        dOut = new DataOutputStream(socket.getOutputStream());

        new Thread(this).start();
    }

    private void processMessage(String msg) throws IOException {
        dOut.writeUTF(msg);
        tf.setText("");
    }

    @Override
    public void run() {
        // receive messages one by one, forever...
        while(true) {
            // get next message
            try {
                String msg = dIn.readUTF();

                // print message to our text window
                ta.append(msg.concat("\n"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
