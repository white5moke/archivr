package org.mintaka5.net;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Hashtable;

public class Server {
    private ServerSocket ss;

    private Hashtable<Socket, DataOutputStream> outputStreams = new Hashtable();

    public Server(int port) throws IOException {
        listen(port);
    }

    private void listen(int port) throws IOException {
        ss = new ServerSocket(port);
        System.out.println("listening on " + ss);

        while(true) {
            Socket s = ss.accept();
            System.out.println("connection from  " + s);

            DataOutputStream dOut = new DataOutputStream(s.getOutputStream());
            outputStreams.put(s, dOut);

            new ServerThread(this, s);
        }
    }

    public Enumeration getOutputStreams() {
        return outputStreams.elements();
    }

    public void sendToAll(String msg) throws IOException {
        synchronized (outputStreams) {
            for(Enumeration e = getOutputStreams(); e.hasMoreElements();) {
                DataOutputStream dOut = (DataOutputStream) e.nextElement();
                dOut.writeUTF(msg);
            }
        }
    }

    public void removeConnection(Socket s) throws IOException {
        synchronized (outputStreams) {
            System.out.println("removing connection to " + s);
            outputStreams.remove(s);

            s.close();
        }
    }

    public static void main(String[] args) throws IOException {
        int port = Integer.parseInt(args[0].strip());

        new Server(port);
    }
}
