package org.mintaka5.nlp;

import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.epub.EpubReader;
import opennlp.tools.sentdetect.*;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.MarkableFileInputStreamFactory;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.TrainingParameters;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

public class NLP {
    public static void main(String[] args) throws IOException {
        Path epubFile = Path.of("C:", "Users", "chris", "Downloads", "the-boy-who-could-change-the-world-the-writings-of--annas-archive.epub");
        EpubReader reader = new EpubReader();
        Book book = reader.readEpub(new FileInputStream(epubFile.toFile()));
        book.getContents().forEach((content) -> {
            /*try {
                String html = new String(content.getData());
                Document doc = Jsoup.parse(html);
                String body = doc.body().text();
                System.out.println(body); */
                Path binPath = Path.of("C:", "Users", "chris", "Downloads", "en-token.bin");
                Path epubTxt = Path.of("C:", "Users", "chris", "value-is-soul", "the-boy-who-could-change-the-world.txt");

            try {
                InputStream modelIn = new FileInputStream(binPath.toFile());
                TokenizerModel model = new TokenizerModel(modelIn);
                Tokenizer tokenizer = new TokenizerME(model);
                String[] tokens = tokenizer.tokenize("Nice job, Aaron!");
                System.out.println(StringUtils.join(tokens, " "));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

                /*
                OutputStream os = new BufferedOutputStream(Files.newOutputStream(epubTxt, StandardOpenOption.CREATE, StandardOpenOption.WRITE));
                os.write(body.getBytes(StandardCharsets.UTF_8), 0, body.length());
                os.close();
                */

                /*ObjectStream<String> lineStream = new PlainTextByLineStream(new MarkableFileInputStreamFactory(epubTxt.toFile()), StandardCharsets.UTF_8);
                SentenceModel model;

                ObjectStream<SentenceSample> sampleStream = new SentenceSampleStream(lineStream);
                model = SentenceDetectorME.train(
                        "eng",
                        sampleStream,
                        new SentenceDetectorFactory("eng", true, null, null),
                        TrainingParameters.defaultParams()
                );

                OutputStream modelOut = new BufferedOutputStream(new FileOutputStream(binPath.toFile()));
                model.serialize(modelOut);
                modelOut.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            } */
        });
    }
}
