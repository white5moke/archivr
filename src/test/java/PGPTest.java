import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.mintaka5.crypto.PGPTool;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;
import java.util.Base64;

public class PGPTest {
    public static void main(String[] args) throws Exception {
        Security.addProvider(new BouncyCastleProvider());

        String passwd = "casey5";

        Path secretPath = Paths.get("C:", "Users", "chris", "value-is-soul", "secret-key-test.asc");
        Path publicPath = Paths.get("C:", "Users", "chris", "value-is-soul", "public-key-test.asc");

        //generate("chris.is.rad@pm.me", passwd, secretPath, publicPath);

        testEncryptDecrypt(publicPath, secretPath, passwd);
    }

    private static void testEncryptDecrypt(Path pPath, Path sPath, String passwd) throws Exception {
        String msg = "Sometimes you just have to not give a fuck anymore. When things are bad, there's only good to look forward to.";
        PGPPublicKeyRing pubRing = PGPTool.importPublicKeyring(pPath.toFile());
        PGPPublicKey pubKey = PGPTool.getEncryptionKey(pubRing);

        byte[] encryptedMsg = encrypt(pPath, msg);
        System.out.println(Base64.getEncoder().encodeToString(encryptedMsg));

        byte[] decryptedMsg = decrypt(sPath, pubKey.getKeyID(), encryptedMsg, passwd);
        System.out.println(new String(decryptedMsg, StandardCharsets.UTF_8));
    }

    private static byte[] decrypt(Path secretPath, long pubKeyId, byte[] encryptedMsg, String passwd) throws Exception {
        PGPSecretKeyRing secRing = PGPTool.importSecretKeyring(secretPath.toFile());
        PGPPrivateKey privKey = PGPTool.getDecryptionKey(secRing, pubKeyId, passwd);

        return PGPTool.decrypt(privKey, encryptedMsg);
    }

    private static byte[] encrypt(Path publicPath, String msg) throws IOException, PGPException {
        PGPPublicKeyRing pubRing = PGPTool.importPublicKeyring(publicPath.toFile());
        PGPPublicKey pubKey = PGPTool.getEncryptionKey(pubRing);

        return PGPTool.encrypt(pubKey, msg.getBytes(StandardCharsets.UTF_8));
    }

    private static void generate(String mail, String passwd, Path secretPath, Path publicPath) throws PGPException, IOException {
        PGPKeyRingGenerator gen = PGPTool.generateKeyRing(mail, passwd);

        PGPTool.exportPublicKey(gen, publicPath.toFile(), true);
        PGPTool.exportSecretKey(gen, secretPath.toFile(), true);
    }
}
